/**
 * Created by ali on 02/03/2017.
 */
var app = angular.module('todolistapp', []);
app.controller('todocontroller', function ($scope) {
    $scope.todolist = [];
    var templist = $scope.todolist;
    $scope.activeitemnum = 0;

    $scope.addtodolist = function () {
        $scope.todolist.push({ text: $scope.todoInput, done: false });
        $scope.todoInput = "";
    };

    $scope.showall = function () {
        $scope.todolist = templist;
    };

    $scope.showactive = function () {
        var oldlist = templist;
        $scope.todolist = [];
        angular.forEach(oldlist, function (value) {
            if (!value.done)
                $scope.todolist.push(value);
        });
    };

    $scope.showacomplated = function () {
        var oldlist = templist;
        $scope.todolist = [];
        angular.forEach(oldlist, function (value) {
            if (value.done)
                $scope.todolist.push(value);
        });
    };

    $scope.removeall = function () {
        $scope.todolist = [];
        templist = [];

    };

 $scope.calcountitem = function () {
        if (!Array.isArray($scope.todolist)) return 0;

        var activeItems = 0;
        $scope.todolist.forEach(value => {
            if (!value.done)
                activeItems++;
        })
       $scope.activeitemnum= activeItems;
    }
/*
    $scope.onChange = function() {
        if (!Array.isArray($scope.todolist)) return 0;

        var activeItems = 0;
        for (const value of $scope.todolist) {
            if (value.done)
                activeItems++;
        }

        $scope.activeItems = activeItems;
    }

    function cal() {
        if (!Array.isArray($scope.todolist)) return 0;

        $scope.activeItems = $scope.todolist.filter(function(value) {
            return value.done;
        }).length;
    }

    function cal() {
        if (!Array.isArray($scope.todolist)) return 0;

        $scope.activeItems = $scope.todolist.filter(value => value.done).length;
    }

    function cal() {
        if (!Array.isArray($scope.todolist)) return 0;

        $scope.activeItems = $scope.todolist.reduce((activeItems, value) => {
            return activeItems + (value.done ? 1 : 0);
        }, 0);
    }*/
});